const io3d = require('3dio')
const configs = require('../configs.js')
const firebaseAdmin = require('firebase-admin')
const nodemailer = require('nodemailer')
const sendGridTransport = require('nodemailer-sendgrid-transport')
const handleError = require('./utils/handle-error.js')

// internals

const db = firebaseAdmin.database()
const mailer = nodemailer.createTransport(sendGridTransport(configs.nodemailer.sendGrid))

// init

module.exports = function onConversionStatusUpdate (rpc) {

  // params from 3d.io API callback
  const conversionId = rpc.params.conversionId

  console.log(`Accepted API request "FloorPlan.onConversionStatusUpdate" with params:`, rpc.params)

  // get conversion status
  getConversionStatusFrom3dio(rpc, conversionId).then(statusData => {
    // update status info in database
    return writeToDatabase(rpc, conversionId, statusData)
  }).then(() => {
    // get all conversion data from database
    return readFromDatabase (rpc, conversionId)
  }).then(conversionData => {
    // send out notifications etc.
    return handleStatusUpdate(rpc, conversionData)
  }).then(() => {
    rpc.end('') // for JSON-RPC2 notifications
    // rpc.sendResult('') // for JSON-RPC2 requests
  }).catch(error => {
    rpc.sendError(error)
  })

}

// private methods

function getConversionStatusFrom3dio (rpc, conversionId) {
  return io3d.floorPlan.getConversionStatus({ conversionId: conversionId }).then(result => {
    console.log(`Received conversion status from 3d.io API`)
    return result
  }).catch(error => {
    return handleError('Error receiving conversion status from 3d.io.', error, rpc)
  })
}

function writeToDatabase (rpc, conversionId, statusData) {
  return db.ref('conversions/' + conversionId).update(statusData).then(result => {
    console.log(`Stored conversion status update to database`)
    return result
  }).catch(error => {
    return handleError('Error writing to database.', error, rpc)
  })
}

function readFromDatabase (rpc, conversionId) {
  return db.ref('conversions/' + conversionId).once('value').then(snapshot => {
    var conversionData = snapshot.exportVal()
    console.log(`Read conversion data from database`)
    return conversionData
  }).catch(error => {
    return handleError('Error reading from database.', error, rpc)
  })
}

function handleStatusUpdate (rpc, conversionData) {
  const status = conversionData.status
  const toEmail = conversionData.customer.email
  const forename = conversionData.customer.forename
  const surname = conversionData.customer.surname
  const tel = conversionData.customer.tel
  const street = conversionData.customer.street
  const zip = conversionData.customer.zip
  const city = conversionData.customer.city
  const adsEmail = conversionData.customer.adsEmail
  const adsTel = conversionData.customer.adsTel

  if (status === 'COMPLETED') {
    console.log(`Floor plan conversion successful`)
    const sceneUrl = io3d.scene.getViewerUrl({ sceneId: conversionData.sceneId })
    const emailBody = `Ihr 3D-Modell ist bereit: ${sceneUrl}`
    const salesEmailBody = `${forename} ${surname} hat eben folgendes 3D-Modell bekommen: ${sceneUrl}. Die Adresse lautet: ${street} ${zip} ${city}. E-Mail-Adresse: ${toEmail}. Telefon-Nummer: ${tel}. Werbe-Erlaubnis per E-Mail erteilt: ${adsEmail}. Werbe-Erlaubnis per Telefon erteilt: ${adsTel}.`

    sendEmailToCustomer(rpc, {
      to: [configs.salesEmail],
      from: configs.fromEmail,
      subject: 'Ein 3D-Modell wurde erstellt',
      text: salesEmailBody,
      html: salesEmailBody
    })

    return sendEmailToCustomer(rpc, {
      to: [toEmail],
      from: configs.fromEmail,
      subject: 'Ihr 3D-Modell ist bereit',
      text: emailBody,
      html: emailBody
    })

  }  else if (status === 'REJECTED') {
    console.log(`Floor plan conversion rejected`)
    // conversion has unfortunately been reject. most likely due to one of the following reasons:
    // NO_FLOORPLAN, UNCLEAR_FLOORPLAN, MULTIPLE_LEVELS, INCLINED_CEILING
    // Read more about converion limitiations: https://3d.io/docs/api/1/basic-3d-model.html
    const emailBody = `Entschuldigen Sie, Ihr Grundriss konnte nicht in ein 3D-Modell umgewandelt werden. Der Grund ist: ${conversionData.rejectionReason}`
    return sendEmailToCustomer(rpc, {
      to: [toEmail],
      from: configs.fromEmail,
      subject: 'Fehler bei der Erstellung des 3D-Modells',
      text: emailBody,
      html: emailBody
    })

  }  else if (status === 'IN_PROGRESS') {
    // not a status update although there should be one
    return handleError(`Error in conversion status update.`, error, rpc)

  }  else {
    // unknown status
    return handleError(`Error: Unknown conversion status: ${status}.`, error, rpc)

  }
}

function sendEmailToCustomer (rpc, email) {
  return mailer.sendMail(email).then(result => {
    console.log(`Sent status update email to customer`)
    return result
  }).catch(error => {
    return handleError('Error sending email to customer.', error, rpc)
  })
}
