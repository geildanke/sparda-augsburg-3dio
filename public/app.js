// configs
var apiUrl = '/'

// get DOM element references
var floorPlanEl = document.querySelector('#floor-plan-url')
var addressEl = document.querySelector('#address')
var emailEl = document.querySelector('#email')
var forenameEl = document.querySelector('#forename')
var surnameEl = document.querySelector('#surname')
var telEl = document.querySelector('#tel')
var streetEl = document.querySelector('#street')
var zipEl = document.querySelector('#zip')
var cityEl = document.querySelector('#city')
var adsEmailEl = document.querySelector('#ads-email')
var adsTelEl = document.querySelector('#ads-tel')
var buttonEl = document.querySelector('.convert-button')
var apiInfoEl = document.querySelector('#api-info')
var buttonClear = document.querySelector('#btn-clear')
var dropzone = document.querySelector('#file-drop-box')
var dropzoneArea = document.querySelector('#dropzone-area')
var uploadField = document.querySelector('#order-field-upload')
var dropzoneRemove = document.querySelector('#dropzone-remove')
var dropPreviews = document.querySelector('#dropzone-drop-previews')
var dropPreviewsPdf = document.querySelector('#dropzone-pdf-message')

// create file drop box
io3d.utils.ui.fileDrop({
  elementId: 'file-drop-box',
  upload: true,
  dragOverCssClass: 'file-drop-box-dragover',
  onInput: function (files) {
    console.log('file upload', files[0])
    dropPreviews.style.backgroundImage = 'url("' + files[0].url + '")'
    dropPreviews.style.display = 'block'
    if (files[0].type === 'application/pdf') { dropPreviews.style.backgroundImage = 'url("pdf.svg")' }
    dropzoneArea.classList.add('not-visible')
    uploadField.classList.add('not-visible')
    floorPlanEl.value = files[0].url
  }
})

dropzoneRemove.addEventListener('click', function() {
  dropPreviews.style.backgroundImage = ''
  dropPreviews.style.display = 'none'
  dropzoneArea.classList.remove('not-visible')
  uploadField.classList.remove('not-visible')
  floorPlanEl.value=''
})

buttonClear.addEventListener('click', function(){
  forenameEl.value = '';
  surnameEl.value = '';
  emailEl.value = '';
  telEl.value = '';
  streetEl.value = '';
  zipEl.value = '';
  cityEl.value = '';
  adsEmailEl.value = '';
  adsTelEl.value = '';
  addressEl.value = '';
  floorPlanEl.value = '';
});

// add event listener to click button
function submitHandler() {
  // start API request
  apiInfoEl.innerHTML = 'Ihre Anfrage wird gesendet ...<br>'
  convertFloorPlanTo3d(floorPlanEl.value, addressEl.value, emailEl.value, forenameEl.value, surnameEl.value, telEl.value, streetEl.value, zipEl.value, cityEl.value, adsEmailEl.value, adsTelEl.value).then(function onSuccess(res) {
    apiInfoEl.innerHTML += 'Ihre Anfrage war erfolgreich. Id: ' + res.result.conversionId + '<br>'
  }).catch(function onError(error) {
    apiInfoEl.innerHTML += 'Bei Ihrer Anfrage ist leider etwas schief gelaufen:' + JSON.stringify(error, null, 2)
    apiInfoEl.innerHTML += '<br>Bitte prüfen Sie Ihre E-Mails für weitere Informationen.'
  })

  return false;
}


// methods
function convertFloorPlanTo3d (floorPlanUrl, address, email, forename, surname, tel, street, zip, city, adsEmail, adsTel) {

  // JSON
  var jsonRpc2Message = {
    jsonrpc: '2.0',
    method: 'Floorplan.requestConversion',
    params: {
      floorPlan: floorPlanUrl,
      address: address,
      email: email,
      forename: forename,
      surname: surname,
      tel: tel,
      street: street,
      zip: zip,
      city: city,
      adsEmail: adsEmail,
      adsTel: adsTel
    },
    id: Math.round(Math.random()*1e20)
  }

  return fetch(apiUrl, {
    method: 'POST',
    body: JSON.stringify( jsonRpc2Message )
  }).then(function(response){
    if (!response.ok) {
      // try to parse response anyway. it might contain a valid JSON error message
      return response.json().then(function onBodyParsed(body){
        return Promise.reject(body)
      })
    } else {
      return response.json()
    }
  })

}
